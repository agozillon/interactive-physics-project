#include "WorldObjectSoundWave.h"
#include <GL\glew.h>
#include <math.h>
#include <glm/gtc/type_ptr.hpp>
#include "PhysicsUtility.h"

WorldObjectSoundWave::WorldObjectSoundWave(float inputPower, float startPower, float red, float green, float blue, glm::vec2 originPoint, glm::vec2 axisDirection)
{
	// calculating the starting colour of the wave based on passed in colour multiplied by inputPower / startPower
	R = red;
	G = green*(inputPower/startPower);
	B = blue*(inputPower/startPower);

	mStartPower = startPower;   // initial unedited sound power in DB  
	mInputPower = inputPower; // input sound power from the previous 'parent' wave/ray that propogated this wave/ray
	soundPower = inputPower;  // current sound power
	
	// setting the current position and origin position of the ray
	currentPosition.x = originPoint.x;
	currentPosition.y = originPoint.y;
	originPosition.x = originPoint.x;
	originPosition.y = originPoint.y;

	// getting the direction of the wave based on the normalized axisDirection 
	direction = glm::normalize(axisDirection);

	// current distance travelled, speed in meters per second and setting the stopMoving bool to false
	distance = 1.0; 
	speedInMPS = 340.0f;
	stopMoving = false;

}

void WorldObjectSoundWave::draw(glm::vec2 scale)
{

	soundPower = mInputPower/distance; // calculating the soundPower based off of the input power / by the distance travelled.
	float percent = soundPower/mStartPower; // calculates the percent of sound power compared to the starting power
	
	glBegin(GL_LINE_STRIP);
	glColor3f(R,G,B);
	glVertex2f(originPosition.x*scale.x, originPosition.y*scale.y); // first point
	glColor3f(1.0*percent,1.0*percent,1.0*percent);  // changing wave colour based on the percent value 
	glVertex2f(currentPosition.x*scale.x, currentPosition.y*scale.y); // second point 
	glEnd();
} 

// updates the sound waves/rays distance by (speedInMPS*deltaTime)*0.0002 (Really should be a scalar number instead
// however this will do for now it stops the rays from instantly speeding off the screen)
void WorldObjectSoundWave::updatePosition(clock_t deltaTime)
{
	distance += ((speedInMPS)*deltaTime)*0.0002;
	currentPosition = originPosition+direction*distance; // calculating the currentPosition of the tip of the ray/wave

	// if the sound power is in the range of 0 (As the way it's calculated it will never actually reach 0)
	// then consider the sound unhearable and set the stopMovingBool, the sound power can be -ve since 
	// the coefficent inverts the wave power every time it hits a wall
	if(soundPower < 0.01f && soundPower > -0.01f)
	{
		updateStopMovingBool(true);
	}
}

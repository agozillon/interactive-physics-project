#ifndef ABSTRACTWORLDOBJECT
#define ABSTRACTWORLDOBJECT
#include <glm/glm.hpp> // including GLM for vec2 variables

// Abstract class for WorldObject's I.E Objects that will be displayed in the world via a draw method
class AbstractWorldObject{
public:
	virtual ~AbstractWorldObject(){return;}   										  // virtual blank destructor for this class
	virtual void draw(glm::vec2 scale) = 0;                                           // pure virtual function for calling the relevant draw functions to draw the object on screen, all derived must implement there own version
	virtual glm::vec2 getPosition() = 0;		                                          // pure virtual function for calling the getPosition function of the objects returns its current position on screen, all derived must implement there own version
};

#endif
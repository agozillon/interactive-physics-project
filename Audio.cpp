#include "Audio.h"


// checks for FMOD errors if one is found it outputs an FMOD based error message and then closes
// down the program
void Audio::ERRCHECK(FMOD_RESULT result)
{
    if (result != FMOD_OK)
    {
        std::printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
        exit(-1);
    }
}


void Audio::initializeSound()
{
	// create an fmod system and initialize it
	result = System_Create(&fmodSystem);
	ERRCHECK(result);
	result = fmodSystem->init(NUM_CHANNELS, FMOD_INIT_NORMAL, 0);
	ERRCHECK(result);
	channel->setVolume(0.5f);

	//create sound from wav file and store it in the variable sound
	result = fmodSystem->createSound("sneezing-1.wav", FMOD_CREATESTREAM | FMOD_SOFTWARE, 0, &sound);
	ERRCHECK(result);
	sound->setMode (FMOD_LOOP_OFF);

	//populate buffer with the data from the wav file
	float freq; FMOD_SOUND_FORMAT format; int channels, bits;
	unsigned int lenMs, lenPcm, lenPcmBytes;					//These variables are populated by the sound function calls
	sound->getDefaults(&freq, 0, 0, 0);							//frequency 
	sound->getFormat(0, &format, &channels, &bits);				//format - FMOD_SOUND_FORMAT_PCM16
	sound->getLength(&lenMs,       FMOD_TIMEUNIT_MS);			//lenMs(length in milliseconds)
	sound->getLength(&lenPcm,      FMOD_TIMEUNIT_PCM);			//lenPcm(PCM samples, related to milliseconds * samplerate / 1000) - 135540
	sound->getLength(&lenPcmBytes, FMOD_TIMEUNIT_PCMBYTES);		//lenPcmBytes(Bytes, related to PCM samples * channels * datawidth (ie 16bit = 2 bytes). ) - 271080
	
	// outputing all the audio variables to console 
	std::cout << "freq " << freq << std::endl;
	std::cout << "channels " << channels << std::endl;
	std::cout << "format " << format << std::endl;
	std::cout << "bits " << bits << std::endl;
	std::cout << "length in MS " << lenMs << std::endl;
	std::cout << "length of Pcm " << lenPcm << std::endl;
	std::cout << "length of PcmBytes " << lenPcmBytes << std::endl;

	const signed int BUF_SIZE = lenPcmBytes; //initialize the buffers size to that of the sound file
	buf_size = BUF_SIZE;
	sound->seekData(0);
	unsigned int bytesRead;
	sound->readData(soundBuffer, buf_size, &bytesRead); //Reads data from an opened sound to a specified pointer, using the FMOD codec created internally.

	for(int i = 0; i < buf_size; i++)
		vecBuffer.push_back(soundBuffer[i]);
	
}

// updates the current FMODSystem
void Audio::updateSound()
{
	result = fmodSystem->update();
	ERRCHECK(result);
}


void Audio::loadNewWav()
{
	// loading in the new sound into the newWav sound object then returning a value to result to
	// check for errors in the creation of the sound
	result = fmodSystem->createSound("bufferData.wav", FMOD_CREATESTREAM | FMOD_SOFTWARE, 0, &newWav);
	ERRCHECK(result);
	newWav->setMode (FMOD_LOOP_OFF); // setting it so the audio doesn't looping
	
	float freq; FMOD_SOUND_FORMAT format; int channels, bits;
	unsigned int lenMs, lenPcm, lenPcmBytes;					//These variables are populated by the sound function calls
	newWav->getDefaults(&freq, 0, 0, 0);						//frequency - 44100.000hz
	newWav->getFormat(0, &format, &channels, &bits);			//format - FMOD_SOUND_FORMAT_PCM16
	newWav->getLength(&lenMs,       FMOD_TIMEUNIT_MS);			//lenMs(length in milliseconds)
	newWav->getLength(&lenPcm,      FMOD_TIMEUNIT_PCM);			//lenPcm(PCM samples, related to milliseconds * samplerate / 1000) - 135540
	newWav->getLength(&lenPcmBytes, FMOD_TIMEUNIT_PCMBYTES);	//lenPcmBytes(Bytes, related to PCM samples * channels * datawidth (ie 16bit = 2 bytes). ) - 271080

	std::cout << "freq " << freq << std::endl;
	std::cout << "channels " << channels << std::endl;
	std::cout << "format " << format << std::endl;
	std::cout << "bits " << bits << std::endl;
	std::cout << "length in MS " << lenMs << std::endl;
	std::cout << "length of Pcm " << lenPcm << std::endl;
	std::cout << "length of PcmBytes " << lenPcmBytes << std::endl;
}

// plays audio stored in sound variable
void Audio::playSound()
{	
	// play sound on available channel
	result = fmodSystem->playSound(FMOD_CHANNEL_FREE, sound, false, &channel);
}

// plays audio stored in newWav variable
void Audio::playNewWav()
{	
	// play sound on available channel
	result = fmodSystem->playSound(FMOD_CHANNEL_FREE, newWav, false, &channel);
}

// writes the audio's PCM data stored in the passed in buffer to a text file
void Audio::writeToTextFile(std::vector<signed short> buff, int newBufferSize, char * fileName)
{
  std::cout << "writing buffer data to file" << std::endl;
  
  // opens file checks if it's open then parses in the data
  pcmDataFile.open(fileName, std::ios::trunc);
  if (pcmDataFile.is_open())
  {
	  for (int i=0; i < newBufferSize; i++)
    pcmDataFile << buff[i] << " ";
    pcmDataFile.close();
  }
  else std::cout << "Unable to open file";	
}

void Audio::SaveToWav(FMOD::Sound *sound)
{
	
    FILE *fp;
    int             channels, bits;
    float           rate;
    unsigned int    lenbytes, lenMs, lenPcm;

    if (!sound)
    {
        return;
    }

	sound->getLength(&lenMs,       FMOD_TIMEUNIT_MS);	   // getting the length of audio in miliseconds
	sound->getLength(&lenPcm,      FMOD_TIMEUNIT_PCM);	   // getting the length of PCM data		
    sound->getFormat  (0, 0, &channels, &bits);			   // getting the number of channels and number of bits(it's a 16 bit WAV file)
    sound->getDefaults(&rate, 0, 0, 0);					   // getting the rate of audio in hertz				
    sound->getLength  (&lenbytes, FMOD_TIMEUNIT_PCMBYTES); // getting the length of audio in bytes
	lenbytes = vecBuffer.size();                           // lenbytes = vector buffer size

    {
        #if defined(WIN32) || defined(_WIN64) || defined(__WATCOMC__) || defined(_WIN32) || defined(__WIN32__)
        #pragma pack(1)
        #endif
        
        /*
            WAV Structures
        */
        typedef struct
        {
	        signed char id[4];
	        int 		size;
        } RiffChunk;
    
        struct
        {
            RiffChunk       chunk           __PACKED;
            unsigned short	wFormatTag      __PACKED;    /* format type  */
            unsigned short	nChannels       __PACKED;    /* number of channels (i.e. mono, stereo...)  */
            unsigned int	nSamplesPerSec  __PACKED;    /* sample rate  */
            unsigned int	nAvgBytesPerSec __PACKED;    /* for buffer estimation  */
            unsigned short	nBlockAlign     __PACKED;    /* block size of data  */
            unsigned short	wBitsPerSample  __PACKED;    /* number of bits per sample of mono data */
        } FmtChunk  = { {{'f','m','t',' '}, sizeof(FmtChunk) - sizeof(RiffChunk) }, 1, channels, (int)rate, (int)rate * channels * bits / 8, 1 * channels * bits / 8, bits } __PACKED;

        struct
        {
            RiffChunk   chunk;
        } DataChunk = { {{'d','a','t','a'}, lenbytes } };

        struct
        {
            RiffChunk   chunk;
	        signed char rifftype[4];
        } WavHeader = { {{'R','I','F','F'}, sizeof(FmtChunk) + sizeof(RiffChunk) + lenbytes }, {'W','A','V','E'} };

        #if defined(WIN32) || defined(_WIN64) || defined(__WATCOMC__) || defined(_WIN32) || defined(__WIN32__)
        #pragma pack()
        #endif

		// opening new file 
        fp = fopen("bufferData.wav", "wb");
       
        /*
            Write out the WAV header.
        */
        fwrite(&WavHeader, sizeof(WavHeader), 1, fp);
        fwrite(&FmtChunk, sizeof(FmtChunk), 1, fp);
        fwrite(&DataChunk, sizeof(DataChunk), 1, fp);
        
		std::cout << "saving buffer as wav file" << std::endl;
			
		// Write buffer to file.
		fwrite(vecBuffer.data(), vecBuffer.size(), 1, fp);

        // close file
        fclose(fp);

    }
}
#include "WorldObjectWall.h"
#include <GL\glew.h>

// WorldObjectWall constructor setting up various passed in parameters 
WorldObjectWall::WorldObjectWall(float Rcoefficient, float red, float green, float blue, float x, float y, float h, float w)
{
	R = red;
	G = green;
	B = blue;
	position.x = x;
	position.y = y;
	height = h;
	width = w;
	reflectionCoefficient = Rcoefficient;
}

// basic function for drawing a square/rectangle from a specific position, also scales it based off of the
// passed in scalar
void WorldObjectWall::draw(glm::vec2 scale)
{
	glColor3f(R,G,B);
	glBegin(GL_POLYGON); 
	  glVertex2f (position.x*scale.x, position.y*scale.y); // first corner
	  glVertex2f ((position.x+width)*scale.x, position.y*scale.y); // second corner
	  glVertex2f ((position.x+width)*scale.x, (position.y+height)*scale.y); // third corner
	  glVertex2f (position.x*scale.x, (position.y+height)*scale.y); // fourth corner
	glEnd();
}

// function that updates the WorldObjectWall's current position
void WorldObjectWall::updatePosition(float x, float y)
{
	position.x = x;
	position.y = y;
}
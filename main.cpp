#include "Game.h"

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

//creating game object then initilizing and starting the run time loop 
int main(int argc, char *argv[])
{
	Game *newGame = new Game();

	newGame->init(); 
	newGame->run();

	delete newGame;

	return 0;
}
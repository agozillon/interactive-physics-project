#ifndef PLAYSTATE
#define PLAYSTATE
#include "abstractState.h"		  // including various headers for use within this main state
#include "AbstractWorldObject.h"
#include "WorldObjectSpeaker.h"
#include "WorldObjectWall.h"
#include "WorldObjectReceiver.h"
#include "WorldObjectSoundWave.h"
#include <vector>
#include "Audio.h"
#include "Label.h"

// structure to hold the result of a sound(various values used to manipulate the sound data), the delay of the sound
// and the multiplier of the sound
struct soundResult
{
	float soundMultiplier;
	float delayInSeconds;
	soundResult(float soundMult,float delay):soundMultiplier(soundMult),delayInSeconds(delay){;}
};

// structure to hold the 'result' of a rays collision, colide = true then its hit a wall collided = false then not.
// also holds the minimumTranslationVector which is the minimum amount it needs to be removed from the wall so that
// it isn't inside the wall
struct result{
	bool colide;
	glm::vec2 minimumTranslationVector;
};

// PlayState class which is derived from the AbstractState class. And thus has implementations of all the inherited virtual
// functions as well as extra functionallity specific to this state.
class PlayState : public AbstractState {
public:
	PlayState(){}								// PlayState constructor
	~PlayState();								// PlayState destructor
	void draw(SDL_Window * window);				// void draw function for drawing/rendering objects to screen 
	void init(Game &context);					// void init function for initilizing variables etc required for this state
	void update(SDL_Window * window);           // void update function for updating various variables, objects etc during run time
	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context); // void function for handling keyboard input 

private:
	glm::vec2 reflectionVector(glm::vec2 axis, glm::vec2 direction);	// calculates the reflection vector of a ray that hits a wall or any vector its called on really.
	result PlayState::rayBoxPointCollision(WorldObjectWall* wall, glm::vec2 point, glm::vec2 direction); // checks for collisions between a point of a vector(current position of the tip), a wall object and the direction of the vector(normalized vector), then returns a result based on the calculations
	bool circlePointCollision(WorldObjectReceiver* listener, glm::vec2 point);              // checks for collisions between a point(tip of a ray in this case) and a WorldObjectReciever which is drawn as a circle and then returns true or false
	void PlayState::createDelay(soundResult* pResult, signed short * nSound, int numOfZeros); // calculates the delay in time it takes for the reciever to here each sound ray and then calculates the number of 0's that need to be put infront of the WAV files buffer data to recreate the delay 
	void addSoundTotalAndAverage(std::vector<signed short*> soundData, std::vector<int> sizeOfBuffer);  // adds up and calculates the average for all of the sound rays sound buffers that hit the reciever, to create a new combined sound/set of data 
	
	// various on screen objects
	WorldObjectWall * walls[4];
	WorldObjectSpeaker * soundOrigin;
	WorldObjectReceiver * soundRecipetant;
	
	// pointer to an Audio object 
	Audio * audioFile;

	// pointer to labels to display various information to the screen
	Label * controls[2];
	Label * soundPressure;
	Label * numberOfRays;
	Label * angleOfSound;
	Label * speedOfSound;

	// vector of vectors of WorldObjectSoundWave pointers 
	std::vector<std::vector<WorldObjectSoundWave*>> sounds;

	// vector of soundResult pointers
	std::vector<soundResult*> results;

	// vector of signed short's to hold the combinedAudioBuffer 
	std::vector<signed short> combinedAudioBuffer;
	
	// variables for holding various time values 
	clock_t currentTime;
	clock_t lastTime;
	clock_t deltaTime;

	// variables for holding other state related values
	int currentNumberOfWaves;
	int numberOfControlLabels;
	int numberOfWalls;
	bool switchControl;
	bool wavesStarted;
	bool newSoundCreated;
	bool averagingOff;
	glm::vec2 worldScale;
	float initialPowerDb;
};

#endif
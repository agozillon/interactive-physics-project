#pragma once
#ifndef NUM_CHANNELS
#define NUM_CHANNELS 3
#endif
#include "fmod.hpp"      // various includes used and required for this Audio loading class
#include "fmod_errors.h"
#include <iostream>
#include <fstream>
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <vector>

#if defined(WIN32) || defined(__WATCOMC__) || defined(_WIN32) || defined(__WIN32__)
    #define __PACKED                         /* dummy */
#else
    #define __PACKED __attribute__((packed)) /* gcc packed */
#endif

// audio class that implements FMOD to load in a wav file, store it and then play it
// also outputs the audio file to a standard text file and can creates a new WAV file via
// the data fed back in
class Audio
{
private:
	FMOD::System *fmodSystem;			// pointer to main FMOD interface system
	FMOD_RESULT result;					// FMOD_RESULT variable can use to test for errors via this
	FMOD::Channel *channel;				// FMOD Channel pointer to hold number of audio channels
	int channels;						// number of audio channels
	FMOD::Sound *sound;					// FMOD Sound pointers to hold the audio in 
	FMOD::Sound *newWav;
	signed int buf_size;				// int to hold the buffer size of the audio data in PCM format
	signed short soundBuffer[374400];   // array of signed shorts to hold audio PCM data is of specific size, must be edited if we change the audio file size etc
	std::vector<signed short>vecBuffer; // another buffer this time using the STL vector class to hold the buffer data
	std::ofstream pcmDataFile;          // output file stream to output the audio PCM data to file
	
public:
	void ERRCHECK(FMOD_RESULT result);							// checks for errors based on return value of FMOD_RESULT
	void initializeSound();										// loads in original wav file and stores the data into the buffer
	void updateSound();											// updates the sound on a per-frame basis only needed for some devices and environments, not used here but left in for future functionallity
	void loadNewWav();											// loads in the new wav file updated by the program, into the newWav variable
	void playSound();											// plays the audio file stored in sound
	void playNewWav();											// plays the audio file stored in newWav
	FMOD::Sound *getSound(){return sound;}						// returns a pointer to sound
	signed short *getBuffer(){ return soundBuffer;}				// returns a pointer to the soundBuffer
	std::vector<signed short> getVecBuffer(){return vecBuffer;}	// returns the vecBuffer data
	void updateVecBuffer(std::vector<signed short> newVecBuffer){vecBuffer = newVecBuffer;}		// updates the vecBuffer with a passed in vector of signed shorts
	void writeToTextFile(std::vector<signed short> buff, int newBufferSize, char * fileName);	// Takes in a buffer, number of elements and file name and saves the data into a new text file
	void SaveToWav(FMOD::Sound *sound);							// Takes in an FMOD::Sound variable to use its format data and writes the user defined buffer data to a wav file
};


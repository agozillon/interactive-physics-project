#ifndef PHYSICSUTILITY
#define PHYSICSUTILITY
#include <glm/glm.hpp> // glm for vec2 type

// basics utility library for very basic Physics functions mostly just basic 
// vector manipulation and angle calculation at the moment, it's not in particular use
// in this project as we switched from using angles to vectors and we decided to use the 
// GLM library instead which has it's own vector operations!
namespace PhysicsUtility {
	float dotProduct2D(glm::vec2 vectorA, glm::vec2 vectorB);											// simple 2D dot prouduct calculating function
	float magnitudeOf2DVector(glm::vec2 vector);														// calculates the magnitude of a 2D vector
	float calculateTheAngleBetweenVectors(glm::vec2 vectorA, glm::vec2 vectorB);						// calculates the angle between 2 vectors
	glm::vec2 vectorSubtraction(glm::vec2 vectorA, glm::vec2 vectorB);							        // subtracts one VectorB from VectorA
	glm::vec2 rotateVectorDirectionLeft(glm::vec2 vector);										        // rotates a 2D vector to the left x = -y, y = x  
	glm::vec2 rotateVectorDirectionRight(glm::vec2 vector);											    // rotates a 2D vector to the right x = y, y = -x
	glm::vec2 normalizeVector(glm::vec2 vector);														// normalizes a passed in vector
	glm::vec2 calculateRightPerpendicular(glm::vec2 vectorA, glm::vec2 vectorB, float lengthOfPerp);    // calculates right perpendicular larger explanation in the .cpp to big for here
	glm::vec2 calculateLeftPerpendicular(glm::vec2 vectorA, glm::vec2 vectorB, float lengthOfPerp);     // calculates left perpendicular larger explanation in the .cpp to big for here

}

#endif
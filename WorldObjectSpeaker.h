#ifndef WORLDOBJECTSPEAKER
#define WORLDOBJECTSPEAKER
#include "AbstractWorldObject.h"

// WorldObjectSpeaker class inherits from AbstractWorldObject and implements extra functionallity
// and the AbstractWorldObject's functionallity to represent the Speaker
class WorldObjectSpeaker : public AbstractWorldObject {
public:
	// WorldObjectSpeaker constructor taking in various variables and setting them up
	WorldObjectSpeaker(float red, float green, float blue, float x, float y, float points, float radi);
	~WorldObjectSpeaker(){}                                               // blank inline WorldObjectSpeaker destructor 
	void draw(glm::vec2 scale);                                           // function for calling the relevant draw functions to draw the object on screen at the relevant passed in scale
	void updatePosition(float xPos, float yPos);						  // function that updates the WorldObjectSpeakers position by the passed in values
	void rotate(float angleR){orientationR+=angleR;}                      // function for rotating/updating the direction the WorldObjectSpeaker is currently 'facing' i.e its orientation for firing the waves
	float getOrientation(){return orientationR;}						  // function for returning the WorldObjectSpeakers current rotation orientation as a float
	glm::vec2 getPosition(){return position;}                             // function for returning the WorldObjectSpeakers position as a vec2
	float getRadius(){return radius;}                                     // function for returning the WorldObjectSpeakers radius

private:
	glm::vec2 position;
	float radius;
	float R; // Red
	float G; // Green 
	float B; // Blue
	float orientationR;
	float numberOfPoints;  // number of points to plot within the circle, less points = worse looking circle more points = better circle, 24 points should however suffice
};

#endif
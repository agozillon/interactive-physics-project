#include "PhysicsUtility.h"

namespace PhysicsUtility {
	
	// simple calculation to get the dot product, parenthasis not needed but helps to show programmer intent 
	float dotProduct2D(glm::vec2 vectorA, glm::vec2 vectorB )
	{
			return ((vectorA.x)*(vectorB.x))+((vectorA.y)*(vectorB.y));
	}

	// if i get wrong values might be because i'm only feeding back non negative
	// magnitudes
	float magnitudeOf2DVector(glm::vec2 vector)
	{
		if(vector.x < 0)
			vector.x = -vector.x;

		if(vector.y < 0)
			vector.y = -vector.y;

		return sqrt((vector.x*vector.x)+(vector.y*vector.y));
	}

	// vector 1 is the vector that is "hitting" the object, vector 2 is the object being hit in our case a wall, 
	float calculateTheAngleBetweenVectors(glm::vec2 vectorA, glm::vec2 vectorB)
	{
		return acos(dotProduct2D(vectorA, vectorB) / (magnitudeOf2DVector(vectorA) * magnitudeOf2DVector(vectorB)))* 57.29577951f;
	}

	// basic vector subtraction: deducts param 1 from param 2
	glm::vec2 vectorSubtraction(glm::vec2 vectorA, glm::vec2 vectorB)
	{
		glm::vec2 tempVec;
		tempVec.x = vectorB.x - vectorA.x;
		tempVec.y = vectorB.y - vectorA.y;

		return tempVec;
	}

	// rotate a vectors direction(after subtracting you get the direction)
	// 90 degrees to the left 
	glm::vec2 rotateVectorDirectionLeft(glm::vec2 vector)
	{
		glm::vec2 rotatedVector;

		rotatedVector.x = -vector.y;
		rotatedVector.y = vector.x;

		return rotatedVector;
	}

	// rotate a vectors direction(after subtracting you get the direction)
	// 90 degrees to the right
	glm::vec2 rotateVectorDirectionRight(glm::vec2 vector)
	{
		glm::vec2 rotatedVector;

		rotatedVector.x = vector.y;
		rotatedVector.y = -vector.x;

		return rotatedVector;
	}

	// basic vector normalization function
	glm::vec2 normalizeVector(glm::vec2 vector)
	{
		glm::vec2 normalizedVector;
		float magnitudeOfVector;
		magnitudeOfVector = magnitudeOf2DVector(vector);

		normalizedVector.x = vector.x / magnitudeOfVector;
		normalizedVector.y = vector.y / magnitudeOfVector;

		return normalizedVector; 
	}

	// this function works by taking two vectors vectorA and vectorB and subtracting B from A
	// to get the vector direction once it has the vectors direction it "rotates" the direction
	// to the right by switching the x with the y and the y with the x and then negating the NEW y/previous X
	// after that it calculates the magnitude of the new vector and then normalizes the X and Y by it to get
	// the unit vector after finding the vector it multiplies the normalized X and Y by a length of your choice
	// and then adds it onto vectorB. HOWEVER this means that the perpendicular will always be calculated/positioned
	// from/on VectorB to position it on vectorA then we would just switch vectorB for vectorA during the unit vector addition/magnification
	// ALSO depending on which way around your A and B are changes the way the Right and Left are A on the left B on the right 
	// means left will always be outwards right always inwards, I also chose lengthOfPerp to just scale it up by 1 as using a proper scaler
	// would increase the value far out of the bounds that we need
	glm::vec2 calculateRightPerpendicular(glm::vec2 vectorA, glm::vec2 vectorB, float lengthOfPerp)
	{
		glm::vec2 vectorDirection;
		glm::vec2 normalizedDirection;
		float magnitude;

		// deduct A from B
		vectorDirection = vectorSubtraction(vectorA, vectorB);

		vectorDirection = rotateVectorDirectionRight(vectorDirection);
		magnitude = magnitudeOf2DVector(vectorDirection);
		normalizedDirection = normalizeVector(vectorDirection);
		vectorDirection.x = vectorB.x + (normalizedDirection.x * lengthOfPerp);
		vectorDirection.y = vectorB.y + (normalizedDirection.y * lengthOfPerp);

		return vectorDirection;
	}

	// similar to the above except it calculates the left sided perpendicular of B via rotating the
	// initial vector direction differently it still switches the x and y around except this time 
	// it negates the Y/ new X
	glm::vec2 calculateLeftPerpendicular(glm::vec2 vectorA, glm::vec2 vectorB, float lengthOfPerp)
	{
		glm::vec2 vectorDirection;
		glm::vec2 normalizedDirection;
		float magnitude;

		vectorDirection = vectorSubtraction(vectorA, vectorB);
		vectorDirection = rotateVectorDirectionLeft(vectorDirection);
		magnitude = magnitudeOf2DVector(vectorDirection);
		normalizedDirection = normalizeVector(vectorDirection);
		vectorDirection.x = vectorB.x + (normalizedDirection.x * lengthOfPerp);
		vectorDirection.y = vectorB.y + (normalizedDirection.y * lengthOfPerp);

		return vectorDirection;
	}
}
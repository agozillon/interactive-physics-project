#include "PlayState.h"
#include "Game.h"
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include "PhysicsUtility.h"
#include <sstream>
#include <iostream>

#define MAGIC_NUM 0.0174532925;


PlayState::~PlayState()
{
	// deleting all of the objects instantiated with new
	for(int i = 0; i < numberOfWalls; i++)
		delete walls[i];

	for(int i = 0; i < numberOfControlLabels; i++)
		delete controls[i];

	for (int i = 0; i < sounds.size(); i++)
	{
		for(int j = 0; j < sounds[i].size();j++)
		{
			delete sounds[i][j];
		}
	}

	delete soundOrigin;
	delete soundRecipetant;
	delete soundPressure;
	delete numberOfRays;
	delete angleOfSound;
	delete speedOfSound;
}

// a simple ray to box collision detection function that also calculates the minimumTranslationVector
// the amount the ray needs to be removed from the box to be outside of it.
result PlayState::rayBoxPointCollision(WorldObjectWall* wall, glm::vec2 point, glm::vec2 direction)
{
	result tmpResult;
	tmpResult.colide = false;
	tmpResult.minimumTranslationVector = glm::vec2(0,0);
	
	float dx;
	float dy;
	
	// getting the minimum and maximum of the walls x axis and y axis
	// more for ease of reading than anything else, also saves multiple 
	// function calls per line
	float minX = wall->getPosition().x;
	float maxX = wall->getPosition().x+wall->getWidth();
	float minY = wall->getPosition().y;
	float maxY = wall->getPosition().y+wall->getHeight();
	
	// if the x on the point is greater than the minimum but less than the maximum
	// then it's within the walls x Axis range
	if (point.x >= minX && point.x <= maxX && point.y >= minY && point.y <= maxY)
	{
		// since the point is in both the X range and Y range of the wall it
		// has colided with the wall set collision to true
		tmpResult.colide = true;

		// at this stage we calculate how far the point needs to be translated to be outside
		// of the colidee, we check if the point.x - the minimum is greater than the 
		// max - point.x, this tells us how far into the object the point is in either direction. 
		// We then take the smallest as our translation distance. We take the smallest as in this case the
		// smallest will put us back outside on the correct side (as the ray shouldn't penetrate
		// farther than the middle of the rect even in the worst case if it did then the algorithm
		// would translate it to the wrong side)
		if (point.x-minX > maxX-point.x)
		{
			dx = maxX-point.x;
		}
		else
		{
			dx = point.x-minX;
		}
		
		// same idea as above except within the Y range
		if (point.y-minY > maxY-point.y)
		{
			dy = maxY-point.y;
		}
		else
		{
			dy = point.y-minY;
		}
			
		// if dy is greater than dx then we can tell that it is penetrating the object
		// on the X axis and not on the Y, as the Y range for the left and right walls
		// are much larger than the X and again the axis it penetrates should have the 
		// smallest value otherwise it has travelled extremely far into the object at least
		// for AABB's and the chances are you should be aiming for a more accurate algorithm 
		// anyway!
		if(dy > dx)
		{
			// set minimum translation vector
			tmpResult.minimumTranslationVector.x = dx;
			tmpResult.minimumTranslationVector.y = 0;

			// if the direction and minimumTranslationVector are > 0 then we can 
			// tell they are going in the same direction (if less than 0 then not
			// going in the same direction) so reverse the direction of the minimum
			// translation vector so it moves it outside of the object. We add the translation
			// vector onto the position not negate it hence the -minimumTranslationVector
			if(glm::dot(direction, tmpResult.minimumTranslationVector) > 0)
			{
				tmpResult.minimumTranslationVector = -tmpResult.minimumTranslationVector;			
			}		
		}
		else
		{
			// set minimum translation vector
			tmpResult.minimumTranslationVector.x = 0;
			tmpResult.minimumTranslationVector.y = dy;

			// if the direction and minimumTranslationVector are > 0 then we can 
			// tell they are going in the same direction (if less than 0 then not
			// going in the same direction) so reverse the direction of the minimum
			// translation vector so it moves it outside of the object. We add the translation		
			// vector onto the position not negate it hence the -minimumTranslationVector
			if(glm::dot(direction, tmpResult.minimumTranslationVector) > 0)
			{
				tmpResult.minimumTranslationVector = -tmpResult.minimumTranslationVector;
			}
		}
	}

	return tmpResult;
}

// basic circle collision check takes in a WorldObjectReceiver and a point and then
// returns true or false based on if the point is inside the circle
bool PlayState::circlePointCollision(WorldObjectReceiver* listener, glm::vec2 point)
{
	float r = listener->getRadius();
	if(point.x >= listener->getPosition().x - listener->getRadius() && point.x <= listener->getPosition().x + listener->getRadius())
	{ 

		if(point.y >= listener->getPosition().y - listener->getRadius() && point.y <= listener->getPosition().y + listener->getRadius())
		{
			
			return true;
		}
	}
	
	
	return false;
}

// initilize function for initilizing variables, objects etc for this class
void PlayState::init(Game &context)
{
	numberOfWalls = 4; 
	float coefficient = -0.9;
	wavesStarted = false;
	newSoundCreated = false;
	numberOfControlLabels = 2;
	averagingOff = false;

	//scale in pixels per metre
	float scale=1/50.0f;
	worldScale = glm::vec2(scale*0.75,scale);

	// instantiating audio file and initilizing the sound and fmod variables
	audioFile = new Audio();
	audioFile->initializeSound();

	// instantiating Label's
	controls[0] = new Label(TTF_OpenFont("MavenPro-Regular.ttf", 16));
	controls[1] = new Label(TTF_OpenFont("MavenPro-Regular.ttf", 16));
	soundPressure = new Label(TTF_OpenFont("MavenPro-Regular.ttf", 16));
	numberOfRays = new Label(TTF_OpenFont("MavenPro-Regular.ttf", 16));
	angleOfSound =  new Label(TTF_OpenFont("MavenPro-Regular.ttf", 16));
	speedOfSound = new Label(TTF_OpenFont("MavenPro-Regular.ttf", 16));

	// north wall
	walls[0] = new WorldObjectWall(coefficient, 0.7, 0.7, 0.7, -67, 39.0, 2.0, 134.0);

	// south wall
	walls[1] = new WorldObjectWall(coefficient,0.7, 0.7, 0.7, -67, -50.0, 2.0, 134.0);

	// west wall
	walls[2] = new WorldObjectWall(coefficient,0.7, 0.7, 0.7, -67, -49.0, 89, 2.0);

	// east wall
	walls[3] = new WorldObjectWall(coefficient,0.7, 0.7, 0.7, 65, -49.0, 89, 2.0);
	
	// instantiating WorldObjectSpeaker and WorldObjectReceiver
	soundOrigin = new WorldObjectSpeaker(1.0, 0.0, 0.0, 0.0, 0.0, 24, 1.0);
	soundRecipetant = new WorldObjectReceiver(0.0, 0.0, 1.0, 0.0, 20.0, 24, 1.0);


	// writing the unedited original sound File to a notepad
	std::cout << "writing original unedited audio to text file";
	audioFile->writeToTextFile(audioFile->getVecBuffer(), audioFile->getVecBuffer().size(), "originalSoundFile");

	// ideally the Power in decibels would change per bit of PCM data however for this
	// program we've chosen to represent it as a flat 194 Decibels for each bit of data
	initialPowerDb = 194.0f;
	switchControl = false;
}

// function for drawing all the various things required for PlayState
void PlayState::draw(SDL_Window * window)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clearing color buffer and depth buffer 

	// drawing the sound waves
	for (int i = 0; i < sounds.size(); i++)
	{
		for(int j = 0; j < sounds[i].size();j++)
		{
			sounds[i][j]->draw(worldScale);
		}
	}
	
	// looping through the walls and drawing them
	for(int i = 0; i < numberOfWalls; i++) 
		walls[i]->draw(worldScale);


	// drawing the sound origin
	soundOrigin->draw(worldScale);

	// drawing the sound recipetant
	soundRecipetant->draw(worldScale);
		
	// draw user interface labels 		
	std::stringstream tempStrStream[4];
	tempStrStream[0] << "  Start Sound Pressure: " << initialPowerDb;
	tempStrStream[1] << "  Speed Of Sound: " << 340.0;
	tempStrStream[2] << " Number Of Rays: " << 199;

	soundPressure->drawTextToTexture(tempStrStream[0].str().c_str(), -0.95, 0.85);
	speedOfSound->drawTextToTexture(tempStrStream[1].str().c_str(), 0.0, 0.85);
	numberOfRays->drawTextToTexture(tempStrStream[2].str().c_str(), -0.50, 0.85);
	controls[0]->drawTextToTexture("WASD to move Emiter/Reciever, Space to Switch Between them, Left and Right Arrows to rotate Emiter, tab key to emit original sound", -0.98, 0.95);
	controls[1]->drawTextToTexture("backspace for edited sound once waves have been calculated, enter fires off the sound waves and Q turns audio averaging on and off", -0.98, 0.90);	
	
	//swapping buffers
	SDL_GL_SwapWindow(window);
}

// simple vector reflection (ReflectRayVector = 2*normal*(dot(rayvector, normal)-rayvector)
glm::vec2 PlayState::reflectionVector(glm::vec2 axis, glm::vec2 direction)
{
	glm::vec2 reflectVector;
	float dot = glm::dot(direction,axis);
	reflectVector = direction -(axis*2.0f*dot);

	return reflectVector;
}

// creating delays in the passed in empty sound buffer based off of the calculated numOfZero's
//also changing all of the soundBuffers values based on each soundWaves relevant soundResult multiplier
void PlayState::createDelay(soundResult* pResult, signed short * nSound, int numOfZeros)
{
	
	// adding on number of 0's to the new sound
	for(int i = 0; i < numOfZeros; i++)
	{
		nSound[i]=0;						
	}

	// getting the end of the new buffer/buffer size based off of the audioFile's vecbuffer
	// size + the number of zeros to be added
	int end = audioFile->getVecBuffer().size()+numOfZeros;

	// looping through the values after the numOfZeros that were added on and adding on the
	// audioFile buffer data(the original sound) edited by the soundMultiplier data stored
	// in pResult soundMultiplier is based off of the reflection coefficent
	for(int i = numOfZeros; i < end; i++)
	{
		nSound[i]=audioFile->getBuffer()[i-numOfZeros]*pResult->soundMultiplier;
	}

	std::cout << "Finished Creating Delays" << std::endl;
}

// takes in a vector of soundBuffers(pointers to signed shorts) and there buffer size and then
// adds them all together and then averages them to get the end combined audio which we can then output to a
// file 
void PlayState::addSoundTotalAndAverage(std::vector<signed short*> soundData, std::vector<int> sizeOfBuffer)
{

	
	bool sorting = true;
	// bubble sort algorithm, sorting biggest bottom [0], smallest top [max]
	// 99% of the time in this simulation the buffer values will be the same size
	// as it's a directional cone of sound and all waves that hit the reciever will have
	// travelled in near enough the same path. But can't be too careful and could alway's
	// change it to a radial sound emitter.
	for(int i = 0; i < soundData.size() && sorting; i++)
	{
		sorting = false;

		for(int j = 0; j < soundData.size()-1; j++)
		{
			if(sizeOfBuffer[j+1] > sizeOfBuffer[j])
			{
				signed short * tmpPTR = soundData[j];
				int tmpSize = sizeOfBuffer[j];

				sizeOfBuffer[j] = sizeOfBuffer[j+1];
				sizeOfBuffer[j+1] = tmpSize;
				soundData[j] = soundData[j+1];
				soundData[j+1] = tmpPTR;

				sorting = true;
			}
		}
	}

	// resize vector based off of the corresponding size of the biggestbuffer
	combinedAudioBuffer.resize(sizeOfBuffer[0]);

	// putting the initial largest soundData(eddited soundWave audio) into the combinedAudioBuffer
	for(int j = 0; j < sizeOfBuffer[0]; j++)
	{
		combinedAudioBuffer.data()[j] = soundData[0][j];
	}

	// adding on the values for every other soundData array onto the combinedAudioBuffer
	for(int i = 1; i < sizeOfBuffer.size(); i++)
	{
		for(int j = 0; j < combinedAudioBuffer.size(); j++)
		{
			combinedAudioBuffer[j]+=soundData[i][j];
		}
	}
	
	// getting the number of sounds that hit the reciever so we can average it
	int numberOfWaves = soundData.size();

	// dividing the combinedAudioBuffer by the soundData vector size to
	// get the average of the combinedAudioBuffer values(largely so we don't
	// go over the maximum PCM value 32767 or minimum -32767) only does this
	// if averagingOff is set to false
	if(averagingOff == false)
	{
		for(int i = 0; i < combinedAudioBuffer.size(); i++)
		{
			combinedAudioBuffer[i] /= numberOfWaves;
		}
	}
}

void PlayState::update(SDL_Window * window)
{
	// getting delta time
	currentTime = clock();
	deltaTime = currentTime - lastTime;
		
	glm::vec2 newpos; // vec2 for the new sound position
	
	// large set of for loops checking every sound wave for collisions with each of the
	// walls and the sound reciever, if it hits a wall we calculate the reflection vector and then 
	// create a new sound wave based off it's direction
	for(int j = 0; j < sounds.size(); j++)
	{
		for(int k = 0; k < sounds[j].size();k++)
		{
			for(int i = 0; i < numberOfWalls; i++)
			{
				if(!sounds[j][k]->getStopMovingBool())
				{
					// creating a tempResult to store the return value from the rayBoxPointCollision function which in itself
					// checks if the ray has collided with a wall and if it has calculates the minimum translation vector
					result tmpResult = rayBoxPointCollision(walls[i], sounds[j][k]->getPosition(),  sounds[j][k]->getDirection());
				
					if (tmpResult.colide) // if the tmpResult returneds collide member is true
					{
						glm::vec2 tmpReflection;

						// normalizes the minimumTranslationVector that was returned in the tmpResult
						// struct as we will be using it for our normal
						glm::vec2 mtv = glm::normalize(tmpResult.minimumTranslationVector);
								
						// getting the reflectionVector via passing in the minimumTranslation vector and
						// vector direction into the reflectionVector mtv is used as the normal
						tmpReflection = reflectionVector(mtv,sounds[j][k]->getDirection());
								
						// newPos is the soundPosition + the minimumTranslationVector * 1.01 
						// the multiplication is so we're 100% sure it's new position is outside the wall
						// not just on the edge
						newpos = sounds[j][k]->getPosition()+tmpResult.minimumTranslationVector*1.01f;
								
						// create new soundWave at the newpos with the direcection as the tmpReflection vector
						sounds[j].push_back(new WorldObjectSoundWave(sounds[j][k]->getSoundPower()*walls[i]->getCoefficient(), initialPowerDb, 1.0, 1.0, 1.0, newpos, tmpReflection));
								
						// setting the StopMovingBool to true to stop the soundWave from moving any further
						sounds[j][k]->updateStopMovingBool(true);
					}
					else
					{
						if(circlePointCollision(soundRecipetant,sounds[j][k]->getPosition())) // checks for circle collisions with reciever and then returns true or false
						{
							sounds[j][k]->updateStopMovingBool(true); // sets the stopMovingBool to true for the sound that hit the reciever

							// gets the distanceCovered by the full soundWave (i.e every vector of vector pointer to WorldObjectSoundWaves is considered a single
							// soundWave regardless of how many WorldObjectSoundWaves there are in the vector as each WorldObjectSoundWave is considered a propogation
							// of the original)
							float distanceCovered=0.0f; 
							
							for(size_t n = 0; n < sounds[j].size(); n++)
							{
								distanceCovered+=sounds[j][n]->getDistance();
							}
									
							// calculates the delay in seconds based off of the distanceCovered/speed Of Sound in Meter per second
							float delayInS = distanceCovered/340.0f;
									
									
							// here we calculate the delay and record final sound power multiplier, to be applied to array of sound bytes.
							float finalMult = sounds[j][k]->getSoundPower()/initialPowerDb;
									
								

							// and record both in our list of results.
							results.push_back(new soundResult(finalMult,delayInS));
						}
					}
				}
			}
		}
	}

		// looping through all of the soundwaves that are currently in existance and checking if they're StopMoving bool is false
		// if it's false then they continue to update the waves position
	for(int i = 0; i < sounds.size(); i++)
	{
		for(int j = 0; j < sounds[i].size();j++)
		{
			if(!sounds[i][j]->getStopMovingBool())
			{
				sounds[i][j]->updatePosition(deltaTime);
			}
		}
	}

	bool finished = false;
	int current = 0;

	// run through arrays and find out if the leading ray has stopped
	for(int i = 0; i < sounds.size(); i++)
	{
		int end = sounds[i].size()-1;
		
		if(sounds[i][end]->getStopMovingBool())
		{
			current++; 
		}
	}

	// all rays have stopped. set finished to true
	if(current==sounds.size() && wavesStarted == true)
	{
		finished = true;
	}
		
	// if resulsts vector size > 0, I.E the soundWaves have hit the receiver
	if(results.size() > 0)
	{
		// if all of the soundWaves have finished there traversal and no new sound has
		// been created
		if(finished == true && newSoundCreated == false)
		{
			// vectors to hold arrays of tmpData and the tmpData's bufferMaxSize
			std::vector<signed short*> tmpData;
			std::vector<int> bufferMaxSize;

				// getting various audioFile values to calculate the required number of 0's to fill a second delay in time
			int             channels, bits;
			float           rate;
			unsigned int    lenbytes, lenMs;
			audioFile->getSound()->getFormat(0, 0, &channels, &bits);             // getting channels and bits of audio
			audioFile->getSound()->getDefaults(&rate, 0, 0, 0);					  // get rate of sound in Hz
			audioFile->getSound()->getLength(&lenbytes, FMOD_TIMEUNIT_PCMBYTES);  // get length of PCM buffer
			audioFile->getSound()->getLength(&lenMs,  FMOD_TIMEUNIT_MS);		  // lenMs(length in milliseconds)
	
			// calculates the below value for each of the results that occured due to soundWaves hitting the receiver
			// calculating the bitsPerMili via the (frequency * number of channels * bits / 8) / 1000 the 1000 is to change
			// it into miliseconds. calculating the delayInMili in int form by multiplying the results delayInSeconds member
			// by 1000. then we push each results bufferMaxSize(Calculated via numOfZeros + lenbytes) and an dynamic array of signed short's
			// based on the bufferMaxSize onto each vector and then we create the delay for each of the results.
			for(int i = 0; i < results.size(); i++)
			{  
				int bitsPerMili = (int)(((int)rate * channels * bits / 8) / 1000);
				int delayInMili = (int)(results[i]->delayInSeconds * 1000);
				int numOfZeros = bitsPerMili % delayInMili;

				bufferMaxSize.push_back(numOfZeros+lenbytes);
				tmpData.push_back(new signed short[numOfZeros+lenbytes]);
				createDelay(results[i], tmpData[i], numOfZeros);
			}

			// calculate the combinedAudioBuffer sound and then update the vector buffer
			// write it to a text file, then save it to a wav, then reload the wav to play it
			// and set newSoundCreated to true!
			addSoundTotalAndAverage(tmpData, bufferMaxSize);
			audioFile->updateVecBuffer(combinedAudioBuffer);

			audioFile->writeToTextFile(audioFile->getVecBuffer(), audioFile->getVecBuffer().size(), "newSound");
			audioFile->SaveToWav(audioFile->getSound());
			audioFile->loadNewWav();
					
			newSoundCreated = true;
		}
	}
	
	lastTime = currentTime;

	draw(window); // calling draw
}

// checks for key presses
void PlayState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		switch( sdlEvent.key.keysym.sym )
		{
			
			// press escape key to quit out of the application 
			case SDLK_ESCAPE:
			SDL_Event quitEvent;
			quitEvent.type = SDL_QUIT;
			SDL_PushEvent(&quitEvent);
			break;

			// press up arrow or W to move the reciever/emiter upwards
			case SDLK_UP:
			case 'w': case 'W': 
				if(switchControl == true)
					soundOrigin->updatePosition(soundOrigin->getPosition().x, soundOrigin->getPosition().y + 0.5f);
				else 
					soundRecipetant->updatePosition(soundRecipetant->getPosition().x, soundRecipetant->getPosition().y + 0.5f);
			break;

			// press left arrow to rotate the Speaker(soundOrigin)'s orientation
			case SDLK_LEFT:
				soundOrigin->rotate(-0.1);
				break;

			// press right arrow to rotate the Speaker(soundOrigin)'s orientation
			case SDLK_RIGHT:
				soundOrigin->rotate(0.1);
				break;

			// press A to move the reciever/emiter to the left
			case 'a': case 'A': 
				if(switchControl == true)
					soundOrigin->updatePosition(soundOrigin->getPosition().x - 0.5f, soundOrigin->getPosition().y);
				else
					soundRecipetant->updatePosition(soundRecipetant->getPosition().x - 0.5f, soundRecipetant->getPosition().y);
			break;

			
		   // press d to move the reciver/emitter downwards
			case 'd': case 'D':
				if(switchControl == true)
					soundOrigin->updatePosition(soundOrigin->getPosition().x + 0.5f, soundOrigin->getPosition().y);
				else
					soundRecipetant->updatePosition(soundRecipetant->getPosition().x + 0.5f, soundRecipetant->getPosition().y);
			break;
			
			// press down arrow or S to move the reciver/emitter downwards
			case SDLK_DOWN:
			case 's': case 'S':
			   	if(switchControl == true)
					soundOrigin->updatePosition(soundOrigin->getPosition().x, soundOrigin->getPosition().y - 0.5f);
				else
					soundRecipetant->updatePosition(soundRecipetant->getPosition().x, soundRecipetant->getPosition().y - 0.5f);
			break;

			// press space to switch control of the sound reciever/emitter
			case SDLK_SPACE:
				if(switchControl == true)
					switchControl = false;
				else 
					switchControl = true;
			break;

			// press tab to play the original audio
			case SDLK_TAB:
				audioFile->playSound();
			break;
			
			
			// press backspace to play the newWav file, the edited file based off of
			// the programs interactions
			case SDLK_BACKSPACE:
				if(newSoundCreated == true)
					audioFile->playNewWav();	
			break;
		
			
			// press any button other than the previous specified buttons and if the sound waves/sound has not already
			// been emitted it shall release and then set the waveStarted boolean to false
			case SDLK_RETURN:
				if(wavesStarted == false)
				{
					std::cout << " Other key pressed, sound wave emitted " << std::endl;

					wavesStarted = true; // sets wave started too true
					currentNumberOfWaves = 199; // number of waves to start with

					// calculates the selected angle deg of the waves to be fired out based off of the orientation
					// and then creates the number of steps to go through based off of the waveAngleDeg/currentNumberOfWaves
					float waveAngleDeg = 90.0f;
					float selectedAngleDeg = soundOrigin->getOrientation()*180/3.14159268f;
					float angleStep = waveAngleDeg/currentNumberOfWaves;

					// calculates the maximum and mimimum angle
					float maxAngle = selectedAngleDeg+waveAngleDeg/2.0;
					float minAngle = selectedAngleDeg-waveAngleDeg/2.0;

					// loops through each calculated step from the min to max and calculates the
					// direction based off of the sin and cosine of the angle in radians and then
					// creates a sound wave object and pushes it onto the vector. This vector is
					// the original sound wave for that vector any propogated child reflections from
					// that wave will be pushed on top of this
					for(float angle = minAngle; angle < maxAngle; angle+=angleStep)
					{
						glm::vec2 direction;
						float angRad = angle*3.14159268f/180.0;
						direction.x = sin(angRad);
						direction.y = cos(angRad);

						// creates a vector of WorldObjectSoundWave pointer's. instantiates and pushes a single WorldObjectSoundWave pointer onto
						// the vector of WorldObjectSoundWave pointer's and then pushes this vector of WorldObjectSoundWave's onto a vector of vector WorldObjectSoundWave
						// pointers. each original sound vector pushed onto the sounds vector of vectors is considered an originating soundWave and any reflections/propogations
						// this soundWave has will be pushed on to the vector of WorldObjectSoundWave pointers.
						std::vector<WorldObjectSoundWave*> sound;
						sound.push_back(new WorldObjectSoundWave( initialPowerDb,  initialPowerDb, 1.0, 1.0, 1.0, soundOrigin->getPosition(), direction));
						sounds.push_back(sound);
					}
				}
			break;

			// press Q to turn audio averaging on and off
			case 'q': case 'Q':
				if (averagingOff == false)
				{
					std::cout << "Averaging Off" << std::endl;
					averagingOff = true;
				}
				else
				{
					std::cout << "Averaging On" << std::endl;
					averagingOff = false;
				}
			break;
			
			default:
			break;
		}
	}

}

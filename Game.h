#ifndef GAME
#define GAME
#include <SDL.h>				// SDL for window creation and holding. as well as creating the OpenGL context 
#include <SDL_ttf.h>			// SDL_ttf for opening and handling the true type font
#include <iostream>				// input output stream for error information
#include "abstractState.h"      // for pointers to AbstractState

// this class contains the program run loop, alongside all of the various application states, it creates and handles
// window destruction and creation. it also handles exit interaction and state switches. And holds the current state.
class Game{
public:
	Game();										// Game constructor 
	~Game();									// Game destructor
	void init();								// void function for initilizing all of this objects variables 
	void run();									// void function for holding the run time loop				
	void setState(AbstractState * newState);    // void function that takes in a pointer to a new state and changes the current state to it
	AbstractState *getState();					// void function that returns a pointer to the current state
	AbstractState *getPlayState();				// void function that returns a pointer to the play state

private:
	void setupRC();								// void function that creates OpenGL context and an SDL window alongside other setups like double buffering and multisampling
	void exitFatalError(char *message);         // void function that exits the program and outputs an passed in error message
	
	// pointers to AbstractState to hold the currentState and the playState
	AbstractState * playState;                
	AbstractState * currentState;
	
	// variables to hold the SDL window and the GLcontext.
	SDL_Window *window;
	SDL_GLContext context;
	
	// true type font to open and test out the true type font on initilization
	TTF_Font * textFont;
};

#endif
#ifndef WORLDOBJECTSOUNDWAVE
#define WORLDOBJECTSOUNDWAVE
#include "AbstractWorldObject.h"
#include <ctime> // ctime for clock_t 

#define MINAUDIOPOWER = 1.0f; // defining the minimum audio power

// WorldObjectSoundWave object inherits from AbstractWorldObject and adds extra functionallity on it
// for representing the sound wave(Ray) and storing sound wave variables
class WorldObjectSoundWave : public AbstractWorldObject {
public:
	// WorldObjectSoundWave constructor takes in various variables to set up the SoundWave
	WorldObjectSoundWave(float inputPower, float startPower, float red, float green, float blue, glm::vec2 originPosition, glm::vec2 axisDirection);
	~WorldObjectSoundWave(){}								                      // blank WorldObjectSoundWave destructor    
	void draw(glm::vec2 scale);													  // function for calling the relevant draw functions to draw the object on screen
	void updatePosition(clock_t deltaTime);										  // function for updating the vector tips current position based off of the passed in delta time
	glm::vec2 getPosition(){return currentPosition;}							  // function for returning the current position/tip of the vector 
	bool getStopMovingBool(){return stopMoving;}								  // function for returning the stopMoving bools state, represents if the wave has stopped moving 
	void updateStopMovingBool(bool change){stopMoving = change;}				  // function for updating the stopMoving bool via the passed in boolean
	float getDistance(){return distance;}                                         // function for returning the distance travelled by the sound wave(real world distance based off of the speed of sound)
	float getSoundPower(){return soundPower;}									  // function for returning the sounds power
	glm::vec2 getDirection(){return direction;}                                   // function for returning the direction of the ray

private:
    glm::vec2 currentPosition;  // tips current position
	glm::vec2 originPosition;   // origin position
	glm::vec2 direction;	    // ray/wave direction
	float R;				    // Red
	float G;				    // Green 
	float B;					// Blue
	float speedInMPS;			// speed of soundWave in meters per second (gets scaled for visual representation)
	float maxDistance;			// max distance the sound wave can travel  
	float distance;				// current distance the sound wave has travelled
	bool stopMoving;			// boolean for checking/setting if the ray has stopped moving
	float soundPower;           // current sound power
	float mStartPower;			// initial unedited sound power in DB 
	float mInputPower;			// input sound power from the previous 'parent' wave/ray that propogated this wave/ray
};

#endif
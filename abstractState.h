#ifndef ABSTRACTSTATE
#define ABSTRACTSTATE

#include <SDL.h> // including SDL so we can use SDL variables in this class

class Game;
// Game.h will still need to be included in the state implementation files

// Abstract game state class, so that various State classes can inherit this as an interface
// to work from 
class AbstractState {
public:
	virtual ~AbstractState() { return; }										// virtual blank destructor for this class
	virtual void draw(SDL_Window * window) = 0;									// pure virtual void draw function for drawing/rendering objects to screen, all derived classes must implement there own version of this function 
	virtual void init(Game &context) = 0;										// pure virtual void init function for initilizing variables etc, all derived classes must implement there own version of this function 
	virtual void update(SDL_Window * window) = 0;						    	// pure virtual void update function for updating variables etc at run time, all derived classes must implement there own version of this function 
	virtual void handleSDLEvent(SDL_Event const &sdlEvent, Game &context) = 0;	// pure virtual void update function for handling various external(keyboard etc) inputs, all derived classes must implement there own version of this function 
};

#endif
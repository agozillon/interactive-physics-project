#ifndef LABEL_H
#define LABEL_H
#include <SDL.h>	 // including the various SDL/GL libraries so that I can use various SDL and GL types and functions
#include <SDL_ttf.h> // SDL true type font library
#include <GL/glew.h> 

// basic "label" creation class, I.E a class that outputs text to screen.
class Label{

public:
	Label(TTF_Font *font);										   // constructor that takes in a TTF_font pointer										  
	~Label();												       // destructor												
	void drawTextToTexture(const char * str, float x, float y);    // draws the texture to screen and accepts x and y positions as well as a string of chars                                        
	
private:
	// textFont to hold the font we wish for this label
	TTF_Font* textFont;
};

#endif
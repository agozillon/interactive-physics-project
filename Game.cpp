#include "game.h"
#include "PlayState.h"

// function for initilizing variables etc on construction of Game
Game::Game()
{
	setupRC();
}

// function for setting the currentState to a newState
void Game::setState(AbstractState * newState)
{
	currentState = newState;
}

// returns an AbstractState pointer to currentState
AbstractState * Game::getState(void)
{
	return currentState;
}

// returns an AbstractState pointer to playState
AbstractState * Game::getPlayState(void)
{
	return playState;
}

// function for exiting the program and outputting an error message in cases of 
// a fatal error
void Game::exitFatalError(char *message)
{
    std::cout << message << " " << SDL_GetError();
    SDL_Quit();
    exit(1);
}

// function for setting up window and OpenGL context
void Game::setupRC()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        exitFatalError("Unable to initialize SDL");

    // Request an OpenGL 2.1 context.
	// If you request a context not supported by your drivers, no OpenGL context will be created
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering

	// Optional: Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
 
    // Create 800x600 window
    window = SDL_CreateWindow("SoundWave Model for Interactive Physical Modelling",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate

	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		exitFatalError("TTF failed to initialise.");

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		exitFatalError("Failed to open font.");

}

// Initialise OpenGL values and game related values and variables
void Game::init()
{
	playState = new PlayState();
	playState->init(*this); 
	currentState = playState;
}

//function run loop, checks keyboard events each loop and calls the currentState's update function
void Game::run() 
{
	bool running = true; // set running to true
	SDL_Event sdlEvent; // variable to detect SDL events

	while (running)	// the event loop
	{
		while (SDL_PollEvent(&sdlEvent))
		{
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
				currentState->handleSDLEvent(sdlEvent, *this);

		}
		currentState->update(window);
	}
}

// destructor for deleting all of this objects variables that require deletion
Game::~Game()
{
	delete playState;
	TTF_CloseFont(textFont);
    SDL_DestroyWindow(window);
	SDL_Quit();
}
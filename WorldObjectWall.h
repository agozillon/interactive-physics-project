#ifndef WORLDOBJECTWALL
#define WORLDOBJECTWALL
#include "AbstractWorldObject.h"

// WorldObjectWall that inherits from AbstractWorldObject implements interface functions inherited 
// from it and extends it to create a Wall Object
class WorldObjectWall : public AbstractWorldObject {
public:
	// constructor for WorldObjectWall
	WorldObjectWall(float Rcoefficient, float red, float green, float blue, float x, float y, float h, float w);
	~WorldObjectWall(){}												  // destructor for WorldObjectWall
	void draw(glm::vec2 scale);                                           // function for calling the relevant draw functions to draw the object on screen
	void updatePosition(float xPos, float yPos);						  // function that updates the WorldObjectWalls position by the passed in values
	glm::vec2 getPosition(){return position;}						      // function for returning the WorldObjectWalls position as a vec2
	float getHeight(){return height;}									  // function for returning the WorldObjectWalls height as a float
	float getWidth(){return width;}										  // function for returning the WorldObjectWalls width as a float
	float getCoefficient(){return reflectionCoefficient;}                 // function for returning the WorldObjectWalls reflection coefficient

private:
	float reflectionCoefficient;
	glm::vec2 position;
	float width;
	float height;
	float R; // Red
	float G; // Green 
	float B; // Blue
};

#endif
#include "WorldObjectReceiver.h"
#include <GL\glew.h> // glVertex etc.
#include <math.h> // for sin and cos

// cosntructor taking in passed in variables and allocating them 
WorldObjectReceiver::WorldObjectReceiver(float red, float green, float blue, float x, float y, float points, float radi)
{
	R = red;
	G = green;
	B = blue;
	position.x = x;
	position.y = y;
	numberOfPoints = points;
	radius = radi;
}

//draws a circle representing the receiver to the passed in scale
void WorldObjectReceiver::draw(glm::vec2 scale)
{
	glColor3f(R,G,B);
	// circle expressed in radians using the radius is two * PI / it by the number of required points
	// then loop through it the specified number of step and use the angle to create each point and 
	// increment the angle
	const float TWOPI = 2*3.14159268;
	const float STEP = TWOPI / numberOfPoints;
	glBegin(GL_POLYGON);
	for(float angle = 0.0; angle < TWOPI; angle +=STEP)
		glVertex2f(((position.x*scale.x)+sin(angle)*radius*scale.x), (position.y*scale.y+cos(angle)*radius*scale.y));
	glEnd();
}

// set the position x and y to the passed in positions
void WorldObjectReceiver::updatePosition(float x, float y)
{
	position.x = x;
	position.y = y;
}
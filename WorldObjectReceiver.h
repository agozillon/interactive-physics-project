#ifndef WORLDOBJECTRECEIVER 
#define WORLDOBJECTRECEIVER
#include "AbstractWorldObject.h"

// (Andrew :: FOR GROUP) receiver and speaker are pretty much the same class at the moment I thought it best
// to have 2 since we may want one or the other to have different properties 

class WorldObjectReceiver : public AbstractWorldObject {
public:
	WorldObjectReceiver(float red, float green, float blue, float x, float y, float points, float radi); // WorldObjectReciever constructor takes in various values to set it up
	~WorldObjectReceiver(){}											  // blank inline WorldObjectReciever destructor
	void draw(glm::vec2 scale);                                           // function for calling the relevant draw functions to draw the object on screen at the relevant passed in scale
	void updatePosition(float xPos, float yPos);						  // function that updates the WorldObjectRecievers position by the passed in values
	glm::vec2 getPosition(){return position;}							  // function that returns the WorldObjectRecievers position
	float getRadius(){return radius;}									  // function that returns the WorldObjectRecievers radius

private:
    glm::vec2 position;
	float radius;
	float R; // Red
	float G; // Green 
	float B; // Blue
	float numberOfPoints; // number of points to plot within the circle, less points = worse looking circle more points = better circle, 24 points should however suffice
};

#endif
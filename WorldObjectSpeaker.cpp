#include "WorldObjectSpeaker.h"
#include <GL\glew.h>
#include <math.h>

// constructor for WorldObjectSpeaker
WorldObjectSpeaker::WorldObjectSpeaker(float red, float green, float blue, float x, float y, float points, float radi)
{
	R = red;
	G = green;
	B = blue;
	position.x = x;
	position.y = y;
	numberOfPoints = points;
	radius = radi;
	orientationR = 0.0f;
}

// function for drawing the WorldObjectSpeaker as a circle with an arrow pointing in the 
// orientation it is facing
void WorldObjectSpeaker::draw(glm::vec2 scale)
{
	glColor3f(R,G,B);
	float ang = orientationR*180.0f/3.14159268f;
	
	// comination of the circle drawing code in the receiver class alongside arrow creation code 
	const float TWOPI = 2*3.14159268;
	const float STEP = TWOPI / numberOfPoints;
	glBegin(GL_POLYGON);
	for(float angle = 0.0; angle < TWOPI; angle +=STEP)
		glVertex2f(position.x*scale.x+(radius*sin(angle)*scale.x), position.y*scale.y+(radius*cos(angle)*scale.y));
	glEnd();

	// translates then rotates then translates back, then creates the triangle/arrow
		glPushMatrix();
			glTranslatef(position.x*scale.x, position.y*scale.y, 0);
			glRotatef(-ang, 0, 0, 1);
			glTranslatef(-position.x*scale.x, -position.y*scale.y, 0);
			glBegin(GL_POLYGON);
				glColor3f(1.0,1.0,1.0);
				glVertex2f(position.x*scale.x,position.y*scale.y+(radius*scale.y));
				glVertex2f(position.x*scale.x-(radius/3.0*scale.x),position.y*scale.y-(radius*scale.y));
				glVertex2f(position.x*scale.x+(radius/3.0*scale.x),position.y*scale.y-(radius*scale.y));
			glEnd();
		glPopMatrix();


}

// function that updates the WorldObjectSpeaker's current position
void WorldObjectSpeaker::updatePosition(float x, float y)
{
	position.x = x;
	position.y = y;
}
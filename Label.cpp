#include "Label.h"
#include "game.h"

// We should be able to detect when errors occur with SDL if there are 
// unrecoverable errors, then we need to print an error message and quit the program
void exitFatalError(char *message)
{
    std::cout << message << " " << SDL_GetError();
    SDL_Quit();
    exit(1);
}

// constructor that uses passed in parameters to set up a textID using the textToTexture method that returns a GLuint
Label::Label(TTF_Font *font)
{
	textFont = font;
}

// destructor 'closes' the opened TTF
Label::~Label()
{
	TTF_CloseFont(textFont);
}

// creates a texture from an SDL_Surface, specifiys the texture bind, wraps and blend paramaters and then
// applys it to a 2D quad. Then deletes the texture and frees up the SDL_Surface 
void Label::drawTextToTexture(const char * str, float x, float y)
{
	SDL_Surface *stringImage;
	SDL_Color colour = { 255, 255, 0 };
	stringImage = TTF_RenderText_Blended(textFont,str,colour);

	if (stringImage == NULL)
		exitFatalError("String surface not created.");

	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;
	
	GLuint texture;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture); 
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, stringImage->w, stringImage->h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);

	// Draw texture here
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glBegin(GL_QUADS);
	  glTexCoord2d(0,1); // Texture has origin at top not bottom
      glVertex3f (x,y, 0.0); // first corner
	  glTexCoord2d(1,1);
      glVertex3f (x+0.002f*stringImage->w, y, 0.0); // second corner
	  glTexCoord2d(1,0);
      glVertex3f (x+0.002f*stringImage->w, y+0.002f*stringImage->h, 0.0); // third corner
	  glTexCoord2d(0,0);
      glVertex3f (x, y+0.002f*stringImage->h, 0.0); // fourth corner
    glEnd();

	glDisable(GL_TEXTURE_2D);

	SDL_FreeSurface(stringImage);
	glDeleteTextures(1, &texture);
}



